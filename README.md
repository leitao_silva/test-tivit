# Solução da Avaliação de conhecimentos - REST - TIVIT


**Solução do teste de avaliação de conhecimentos em REST para a vaga de desenvolvedor python para a empresa TIVIT**


## Ferramentas utilizadas na solução: ##

* [Django Python Web Framework](https://www.djangoproject.com/)
* [Django REST Framework](http://www.django-rest-framework.org/)
* [Swagger](http://swagger.io/)


## Instruções de Execução ##

1. Clone o repositório

> ```git clone https://bitbucket.org/leitao_silva/test-tivit.git ```

1. Conceder permissões de execução nos scripts

> ```chmod +x run_server.sh```

> ```chmod +x run_tests.sh```

1. Executar os testes

> ```./run_tests.sh```

1. Rodar o servidor local

> ```./run_server.sh ```

## Links da Aplicação ##

* [Documentação](http://127.0.0.1:8000/)
* [API](http://127.0.0.1:8000/api)
* [Documentação da API](http://127.0.0.1:8000/api/docs/)
* [Administrador](http://127.0.0.1:8000/admin)

**username**: tivit
**password**: test12345