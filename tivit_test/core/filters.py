from django_filters.rest_framework import FilterSet
from .models import Feira


class FeiraFilter(FilterSet):
    class Meta:
        model = Feira
        fields = ('distrito', 'regiao_5', 'nome_feira', 'bairro')
