from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from .views import FeiraViewSet


router = routers.DefaultRouter()
router.register(r'feiras', FeiraViewSet)

schema_view = get_swagger_view(title='Feiras Livres API')

urlpatterns = [
    url(r'^docs/', schema_view),
]

urlpatterns += router.urls
