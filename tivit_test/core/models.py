# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models


class Feira(models.Model):
    id = models.AutoField(verbose_name=u'Identificador', primary_key=True,
                          help_text=u'Número de identificação do estabelecimento georreferenciado por SMDU/Deinfo')
    long = models.CharField(verbose_name=u'Longitude', max_length=10,
                            help_text=u'Longitude da localização do estabelecimento no território do Município, conforme MDC')
    lat = models.CharField(verbose_name=u'Latitude', max_length=10,
                           help_text=u'Latitude da localização do estabelecimento no território do Município, conforme MDC')
    set_cens = models.CharField(verbose_name=u'Setor censitário', max_length=15,
                                help_text=u'Setor censitário conforme IBGE')
    area_p = models.CharField(verbose_name=u'Área de ponderação', max_length=13,
                              help_text=u'Área de ponderação (agrupamento de setores censitários) conforme IBGE 2010')
    cod_dist = models.CharField(verbose_name=u'Código do distrito conforme IBGE', max_length=9,
                                help_text=u'Código do Distrito Municipal conforme IBGE')
    distrito = models.CharField(verbose_name=u'Distrito municipal', max_length=18,
                                help_text=u'Nome do Distrito Municipal')
    cod_sub_pref = models.CharField(verbose_name=u'Código da Subprefeitura', max_length=2,
                                    help_text=u'Código de cada uma das 31 Subprefeituras (2003 a 2012)')
    sub_pref = models.CharField(verbose_name=u'Subprefeitura', max_length=25,
                                help_text=u'Nome da Subprefeitura (31 de 2003 até 2012)')
    regiao_5 = models.CharField(verbose_name=u'Região conforme divisão do Município em 5 áreas', max_length=6,
                                help_text=u'Região conforme divisão do Município em 5 áreas')
    regiao_8 = models.CharField(verbose_name=u'Região conforme divisão do Município em 8 áreas', max_length=7,
                                help_text=u'Região conforme divisão do Município em 8 áreas')
    nome_feira = models.CharField(verbose_name=u'Nome da feira livre', max_length=30,
                                  help_text=u'Denominação da feira livre atribuída pela Supervisão de Abastecimento')
    registro = models.CharField(verbose_name=u'Registro da feira livre', max_length=6,
                                help_text=u'Número do registro da feira livre na PMSP')
    logradouro = models.CharField(verbose_name=u'Logradouro', max_length=34,
                                  help_text=u'Nome do logradouro onde se localiza a feira livre')
    numero = models.CharField(verbose_name=u'Número', max_length=5,
                              help_text=u'Um número do logradouro onde se localiza a feira livre')
    bairro = models.CharField(verbose_name=u'Bairro', max_length=20, help_text=u'Bairro de localização da feira livre')
    referencia = models.CharField(verbose_name=u'Ponto de referência', max_length=24,
                                  help_text=u'Ponto de referência da localização da feira livre')

    class Meta:
        verbose_name = u'Feira Livre'
        verbose_name_plural = u'Feiras Livres'

    def __unicode__(self):
        return self.nome_feira
