from django.test import TestCase
from .models import Feira


def create():
    return Feira.objects.create(long='-46550164',
                                lat='-23558733',
                                set_cens='355030885000091',
                                area_p='3550308005040',
                                cod_dist='87',
                                cod_sub_pref='26',
                                distrito='VILA FORMOSA',
                                sub_pref='ARICANDUVA-FORMOSA-CARRAO',
                                regiao_5='Leste',
                                regiao_8='Leste 1',
                                nome_feira='VILA FORMOSA',
                                registro='4041-0',
                                logradouro='RUA MARAGOJIPE',
                                numero='S/N',
                                bairro='VL FORMOSA',
                                referencia='TV RUA PRETORIA')


def create_json():
    return dict(long='-46550164',
                lat='-23558733',
                set_cens='355030885000091',
                area_p='3550308005040',
                cod_dist='87',
                cod_sub_pref='26',
                distrito='VILA FORMOSA',
                sub_pref='ARICANDUVA-FORMOSA-CARRAO',
                regiao_5='Leste',
                regiao_8='Leste 1',
                nome_feira='VILA FORMOSA',
                registro='4041-0',
                logradouro='RUA MARAGOJIPE',
                numero='S/N',
                bairro='VL FORMOSA',
                referencia='TV RUA PRETORIA')


class FeiraTestCase(TestCase):
    def setUp(self):
        create()

    def test_creation_feira(self):
        f = create()
        self.assertTrue(isinstance(f, Feira))
        self.assertEqual(f.__unicode__(), f.nome_feira)

    def test_list_feiras_response_code(self):
        resp = self.client.get('/api/feiras/')
        self.assertEqual(resp.status_code, 200)

    def test_add_feira_response_code(self):
        data = create_json()
        resp = self.client.post('/api/feiras/', data=data)
        self.assertEqual(resp.status_code, 201)

    def test_view_feira_response_code(self):
        resp = self.client.get('/api/feiras/1/')
        self.assertEqual(resp.status_code, 200)

    def test_delete_feira_response_code(self):
        resp = self.client.delete('/api/feiras/1/')
        self.assertEqual(resp.status_code, 204)


from tastypie.test import ResourceTestCase


class FeiraResourceTest(ResourceTestCase):
    def test_list_feiras_response_type(self):
        resp = self.api_client.get('/api/feiras/', format='json')
        self.assertValidJSONResponse(resp)

    def test_view_feira_response_code(self):
        f = create()
        resp = self.api_client.get('/api/feiras/1/', format='json')
        self.assertValidJSONResponse(resp)
