from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from .models import Feira
from .serializers import FeiraSerializer
from .filters import FeiraFilter


class FeiraViewSet(viewsets.ModelViewSet):
    queryset = Feira.objects.all()
    serializer_class = FeiraSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = FeiraFilter

