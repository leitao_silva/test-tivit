from django.conf import settings


def handle_uploaded_file(file):
    destination = open(settings.UPLOAD_AT + file.name, 'wb+')
    for chunk in file.chunks():
        destination.write(chunk)
    destination.close()
    return file