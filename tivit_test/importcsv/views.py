from django.http import HttpResponse
from django.shortcuts import render
from adaptor.model import CsvDataException
from .models import FeiraCsvModel
from .forms import UploadFeiraForm
from utils.file_utils import handle_uploaded_file


def import_csv(request):
    if request.POST:
        form = UploadFeiraForm(request.POST, request.FILES)
        if form.is_valid():
            file_name = handle_uploaded_file(request.FILES['file'])

            try:
                FeiraCsvModel.import_from_file(file_name)
            except CsvDataException:
                return HttpResponse('One or many lines has import errors')

            return HttpResponse('Import complete successfully')
    else:
        form = UploadFeiraForm()

    context = dict(form=form)
    return render(request, 'import_csv.html', context)
