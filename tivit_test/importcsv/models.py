# from __future__ import unicode_literals

from adaptor.model import CsvModel
from adaptor.fields import IntegerField, CharField
from core.models import Feira


class FeiraCsvModel(CsvModel):
    id = IntegerField()
    long = CharField()
    lat = CharField()
    set_cens = CharField()
    area_p = CharField()
    cod_dist = CharField()
    distrito = CharField()
    cod_sub_pref = CharField()
    sub_pref = CharField()
    regiao_5 = CharField()
    regiao_8 = CharField()
    nome_feira = CharField()
    registro = CharField()
    logradouro = CharField()
    numero = CharField()
    bairro = CharField()
    referencia = CharField()

    class Meta:
        dbModel = Feira
        delimiter = ","
        has_header = True
        silent_failure = True
        update = {'keys': ['id']}

