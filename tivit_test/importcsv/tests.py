from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings
from .forms import UploadFeiraForm


class ImportCSVTestCase(TestCase):

    def test_valid_form(self):
        upload_file = open(settings.UPLOAD_AT + 'CSV_TEST.csv', 'rb')
        file_dict = {'file': SimpleUploadedFile(upload_file.name, upload_file.read())}
        form = UploadFeiraForm({}, file_dict)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form = UploadFeiraForm({}, {})
        self.assertFalse(form.is_valid())

    def test_invalid_format_form(self):
        upload_file = open(settings.UPLOAD_AT + 'TXT_TEST.txt', 'rb')
        post_dict = {'title': 'Test Title'}
        file_dict = {'file': SimpleUploadedFile(upload_file.name, upload_file.read())}
        form = UploadFeiraForm(post_dict, file_dict)
        self.assertFalse(form.is_valid())

    def test_import_view(self):
        upload_file = open(settings.UPLOAD_AT + 'CSV_TEST.csv', 'rb')
        resp = self.client.post('/import-csv', {'file': upload_file})
        self.assertEqual(resp.status_code, 200)
