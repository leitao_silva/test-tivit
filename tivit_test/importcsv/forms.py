# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

from django import forms


class UploadFeiraForm(forms.Form):
    file = forms.FileField()

    def clean_file(self):
        file = self.cleaned_data.get('file')
        ext = file.name.split('.')[-1]
        if ext != 'csv':
            raise forms.ValidationError('invalid file format')
        return self.cleaned_data['file']
